-- Textures
local earth_texture
local crosshair_texture
local green_shot_texture
local cannon_texture
local asteroid_texture
local ufo_texture
local three_time_power_up_texture

-- Sounds
local shot_sound1
local shot_sound2
local explosion_sound
local power_up_collect_sound

-- Texts
local point_text

-- Power-Ups
local power_ups = {}
local power_up_count = 0

-- Bullets
local bullets = {}
local bullet_count = 0
local bullet_speed = 230.0
local time_since_last_shot = 0.0
local time_for_new_shot = 0.3
local can_shoot = true

-- Game state
local health = 100.0
local points = 0
local power_up_multiplier = 1

-- Cannon
local cannon_light_up_time = 0.2
local current_light_time = 0.0
local cannon_should_light_up = false

-- Asteroids
local asteroids = {}
local asteroid_count = 0
local asteroid_speed = 150

local cleanup_time = 2.0
local time_since_last_cleanup = 0.0
local explosions = {}
local explosion_count = 0

local earth_is_blinking = false
local current_earth_blink_time = 0.0
local earth_blink_time = 0.1
local current_blink_count = 0
local blinking_count = 4

-- Screen shake
local t, shakeDuration, shakeMagnitude = 0, -1, 0

local time_until_next_asteroid_spawn = 0.0

local random_number_generator = love.math.newRandomGenerator()

function math.normalize(x,y) local l=(x*x+y*y)^.5 if l==0 then return 0,0,0 else return x/l,y/l,l end end

function math.angle(x1, y1, x2, y2) return math.atan2(y2 - y1, x2 - x1) end

function math.rotate(x, y, degrees)
    result_x = x * math.cos(degrees) - y * math.sin(degrees)
    result_y = x * math.sin(degrees) + y * math.cos(degrees)
    return result_x, result_y
end

function loadTexture(filename)
    texture = love.graphics.newImage(filename)
    texture:setFilter("nearest", "nearest")
    return texture
end

function love.load()
	random_number_generator:setSeed(os.time())
    love.window.setMode(1000, 800, { resizable = false, vsync = false })

    earth_texture = loadTexture("assets/textures/earth.png")
    crosshair_texture = loadTexture("assets/textures/crosshair.png")
    green_shot_texture = loadTexture("assets/textures/green_shot.png")
    cannon_texture = loadTexture("assets/textures/cannon.png")
    asteroid_texture = loadTexture("assets/textures/asteroid.png")
    ufo_texture = loadTexture("assets/textures/ufo.png")
    three_time_power_up_texture = loadTexture("assets/textures/3_time_power_up.png")

    shot_sound = love.audio.newSource("assets/sounds/shot.wav", "static")
    shot_sound2 = love.audio.newSource("assets/sounds/shot.wav", "static")
    explosion_sound = love.audio.newSource("assets/sounds/explosion.wav", "static")
    power_up_collect_sound = love.audio.newSource("assets/sounds/power_up_collect.wav", "static")
    love.mouse.setVisible(false)
    spawnPowerUp()
end

function love.update(delta)
    if t < shakeDuration then
        t = t + delta
    end

    if time_until_next_asteroid_spawn <= 0.0 then
        spawnNewAsteroid()
        time_until_next_asteroid_spawn = random_number_generator:random(100.0, 200.0) / 100
    end
    time_until_next_asteroid_spawn = time_until_next_asteroid_spawn - delta

    incrementAnimations(delta)
    incrementExplosions(delta)

    if can_shoot == false then
        if time_since_last_shot >= time_for_new_shot then
            time_since_last_shot = 0.0
            can_shoot = true
        end
        time_since_last_shot = time_since_last_shot + delta
    end

    -- Bullets
    for i = 0, bullet_count - 1 do
        if bullets[i][0] then
            for j = 0, asteroid_count - 1 do
                if asteroids[j].active and CheckCollision(bullets[i][1], bullets[i][2], 50, 50, asteroids[j].position.x, asteroids[j].position.y, 50, 50) then
                    asteroids[j].active = false
                    bullets[i][0] = false
                    points = points + 50
                    spawnExplosion(102, 51, 0, asteroids[j].position.x, asteroids[j].position.y)
                end
            end
            bullets[i][1] = bullets[i][1] + bullets[i][3] * bullet_speed * delta
            bullets[i][2] = bullets[i][2] + bullets[i][4] * bullet_speed * delta
            -- Check if the bullet is out of bounds and flag it for cleanup if it is
            if bullets[i][1] < -10 or bullets[i][1] > love.graphics.getWidth() + 10 or bullets[i][2] < -10 or bullets[i][2] > love.graphics.getHeight() + 10 then
                bullets[i][0] = false
            end
        end
    end

    -- Asteroids
    for i = 0, asteroid_count - 1 do
        if asteroids[i].active then
            asteroids[i].position.x = asteroids[i].position.x + asteroids[i].direction.x * asteroid_speed * delta
            asteroids[i].position.y = asteroids[i].position.y + asteroids[i].direction.y * asteroid_speed * delta
            -- Check if the bullet is out of bounds and flag it for cleanup if it is
            if asteroids[i].position.x < -10 or asteroids[i].position.x > love.graphics.getWidth() + 10 or asteroids[i].position.y < -10 or asteroids[i].position.y > love.graphics.getHeight() + 10 then
                asteroids[i].active = false
            end
        end
    end

    if earth_is_blinking then
        if current_earth_blink_time >= earth_blink_time then
            current_blink_count = current_blink_count + 1
            if current_blink_count > blinking_count then
                earth_is_blinking = false
                current_blink_count = 0
            end
            current_earth_blink_time = 0.0
        end
        current_earth_blink_time = current_earth_blink_time + delta
    end

    -- Check earth collision
    earth_x = love.graphics.getWidth() / 2
    earth_y = love.graphics.getHeight() / 2

    for j = 0, asteroid_count - 1 do
        if asteroids[j].active and CheckCollision(earth_x - 64, earth_y - 64, 128, 128, asteroids[j].position.x - 32, asteroids[j].position.y - 32, 64, 64) then
            asteroids[j].active = false
            startShake()
            spawnExplosion(102, 51, 0, asteroids[j].position.x, asteroids[j].position.y)
            startEarthBlinking()
            decreaseHealth(10)
        end
    end

    for i = 0, explosion_count - 1 do
        if explosions[i].active then
            for j = 0, explosions[i].particle_count - 1 do
                explosions[i].particles[j].position.x = explosions[i].particles[j].position.x + explosions[i].particles[j].direction.x * explosions[i].particle_speed * delta
                explosions[i].particles[j].position.y = explosions[i].particles[j].position.y + explosions[i].particles[j].direction.y * explosions[i].particle_speed * delta
            end
        end
    end

    x, y = love.mouse.getPosition()
    
    for i = 0, power_up_count - 1 do
        if power_ups[i].active and CheckCollision(x, y, 30, 30, power_ups[i].position.x, power_ups[i].position.y, 32, 32) then
            collectPowerUp(power_ups[i].type)
            power_ups[i].active = false
        end
    end

    if time_since_last_cleanup >= cleanup_time then
        cleanup()
    end

    if cannon_should_light_up then
        current_light_time = current_light_time + delta
        if current_light_time >= cannon_light_up_time then
            current_light_time = 0.0
            cannon_should_light_up = false
        end
    end

    if love.mouse.isDown(1) and can_shoot then
        shoot(x, y)
    end
end

function collectPowerUp(type)
    if type == 'x3' then
        power_up_multiplier = 3
    end
    power_up_collect_sound:play()
end

function spawnPowerUp()
    power_up = {}
    power_up.active = true
    power_up.type = "x3" -- @Incomplete
    power_up.position = {}
    power_up.position.x = random_number_generator:random(10, love.graphics.getWidth() - 10)
    power_up.position.y = random_number_generator:random(10, love.graphics.getHeight() - 10)
    power_ups[power_up_count] = power_up
    power_up_count = power_up_count + 1
end

function incrementExplosions(delta)
    for i = 0, explosion_count - 1 do
        if explosions[i].active then
            if explosions[i].current_time >= explosions[i].total_time then
                explosions[i].active = false
            else
                explosions[i].current_time = explosions[i].current_time + delta
            end
        end
    end
end

function incrementAnimations(delta)
    for i = 0, asteroid_count - 1 do
        asteroids[i].animation.currentTime = asteroids[i].animation.currentTime + delta
        if asteroids[i].animation.currentTime >= asteroids[i].animation.duration then
            asteroids[i].animation.currentTime = asteroids[i].animation.currentTime - asteroids[i].animation.duration
        end
    end
end

function love.draw()
    if t < shakeDuration then
        local dx = love.math.random(-shakeMagnitude, shakeMagnitude)
        local dy = love.math.random(-shakeMagnitude, shakeMagnitude)
        love.graphics.translate(dx, dy)
    end
    -- Draw earth
    if earth_is_blinking then
        if current_blink_count % 2 == 0 then
            love.graphics.setColor(255, 255, 255)
        else
            love.graphics.setColor(255, 0, 0)
        end
    else
        love.graphics.setColor(255, 255, 255)
    end

    earth_scale = 4
    half_earth_size = 32 * earth_scale / 2
    love.graphics.draw(earth_texture, love.graphics.getWidth() / 2 - half_earth_size, love.graphics.getHeight() / 2 - half_earth_size, 0, 4, 4)
    love.graphics.setColor(255, 255, 255)

    x, y = love.mouse.getPosition()

    -- Draw cannon
    earth_x = love.graphics.getWidth() / 2
    earth_y = love.graphics.getHeight() / 2
    x_direction, y_direction = math.normalize((x - earth_x), (y - earth_y))
    
    cannon_x = earth_x + x_direction * 85
    cannon_y = earth_y + y_direction * 85

    if cannon_should_light_up then
        love.graphics.setColor(255, 0, 0)
    else
        love.graphics.setColor(255, 255, 255)
    end

    love.graphics.draw(cannon_texture, cannon_x, cannon_y, math.angle(earth_x, earth_y, cannon_x, cannon_y), 4, 4, 8, 8)
    love.graphics.setColor(255, 255, 255)

    -- Draw bullets
    for i = 0, bullet_count - 1 do
        if bullets[i][0] then
            love.graphics.draw(green_shot_texture, bullets[i][1] - 16, bullets[i][2] - 16, 0, 2, 2)
        end
    end

    -- Draw hazards
    for i = 0, asteroid_count - 1 do
        if asteroids[i].active then
            local sprite_num = math.floor(asteroids[i].animation.currentTime / asteroids[i].animation.duration * #asteroids[i].animation.quads) + 1
            love.graphics.draw(asteroids[i].animation.spriteSheet, asteroids[i].animation.quads[sprite_num], asteroids[i].position.x, asteroids[i].position.y, math.angle(asteroids[i].position.x, asteroids[i].position.y, earth_x, earth_y), 2)
        end
    end

    for i = 0, explosion_count - 1 do
        if explosions[i].active then
            love.graphics.setColor(explosions[i].color.r, explosions[i].color.g, explosions[i].color.b)
            for j = 0, explosions[i].particle_count - 1 do
                love.graphics.rectangle("fill", explosions[i].particles[j].position.x, explosions[i].particles[j].position.y, 5, 5)
            end
        end
    end

    love.graphics.setColor(255, 255, 255)
    for i = 0, power_up_count - 1 do
        if power_ups[i].active then
                love.graphics.draw(three_time_power_up_texture, power_ups[i].position.x, power_ups[i].position.y, 0, 2, 2)
        end
    end
    
    -- Draw crosshair
    love.graphics.draw(crosshair_texture, x, y, 0, 2, 2)

    -- Draw life bar
    love.graphics.rectangle("fill", love.graphics.getWidth() / 2 - 300, love.graphics.getHeight() - 100, 600, 40)
    love.graphics.setColor(0, 200, 100)
    love.graphics.rectangle("fill", love.graphics.getWidth() / 2 - 295, love.graphics.getHeight() - 95, 590 / 100.0 * health, 30)

    love.graphics.setColor(255, 255, 255)
    love.graphics.print(points, love.graphics.getWidth() / 2, 50, 0, 4, 4)
end

function love.mousepressed(x, y, button)
    if can_shoot then
        shoot(x, y)
    end
end

-- Game functions
function cleanup()
    new_bullet_count = 0
    new_bullet_list = {}
    for i = 0, bullet_count - 1 do
        if bullets[i][0] then
            new_bullet_list[new_bullet_count][0] = bullets[i]
            new_bullet_count = new_bullet_count + 1
        end
    end
    bullets = new_bullet_list
    bullet_count = new_bullet_count

    new_asteroid_count = 0
    new_asteroid_list = {}
    for i = 0, asteroid_count - 1 do
        if asteroids[i].active == false then
            new_asteroid_list[new_asteroid_count][0] = asteroids[i]
            new_asteroid_count = new_asteroid_count + 1
        end
    end
    asteroids = new_asteroid_list
    asteroid_count = new_asteroid_count

    new_explosion_count = 0
    new_explosion_list = {}
    for i = 0, explosion_count - 1 do
        if explosions[i].active == false then
            new_explosion_list[new_explosion_count][0] = explosions[i]
            new_explosion_count = new_explosion_count + 1
        end
    end
    explosions = new_explosion_list
    explosion_count = new_explosion_count

    time_since_last_cleanup = 0.0
end

function decreaseHealth(health_decrease)
    health = health - health_decrease
end

function shoot(click_position_x, click_position_y)
    if shot_sound:isPlaying() then
        shot_sound2:play()
    else
        shot_sound:play()
    end

    earth_x = love.graphics.getWidth() / 2
    earth_y = love.graphics.getHeight() / 2

    x_direction, y_direction = math.normalize((click_position_x - earth_x), (click_position_y - earth_y))
    x_direction2, y_direction2 = math.rotate(x_direction, y_direction, 0.7)
    x_direction3, y_direction3 = math.rotate(x_direction, y_direction, -0.7)

    cannon_x = earth_x + x_direction * 90
    cannon_y = earth_y + y_direction * 90

    earth_scale = 4
    half_earth_size = 32 * earth_scale / 2 
    bullets[bullet_count] = {}
    bullets[bullet_count][0] = true
    bullets[bullet_count][1] = cannon_x
    bullets[bullet_count][2] = cannon_y
    bullets[bullet_count][3] = x_direction
    bullets[bullet_count][4] = y_direction
    bullet_count = bullet_count + 1

    if power_up_multiplier > 1 then
        bullets[bullet_count] = {}
        bullets[bullet_count][0] = true
        bullets[bullet_count][1] = cannon_x
        bullets[bullet_count][2] = cannon_y
        bullets[bullet_count][3] = x_direction2
        bullets[bullet_count][4] = y_direction2
        bullet_count = bullet_count + 1

        bullets[bullet_count] = {}
        bullets[bullet_count][0] = true
        bullets[bullet_count][1] = cannon_x
        bullets[bullet_count][2] = cannon_y
        bullets[bullet_count][3] = x_direction3
        bullets[bullet_count][4] = y_direction3
        bullet_count = bullet_count + 1
    end

    can_shoot = false
    cannon_should_light_up = true
    startShake()
end

function startEarthBlinking()
    earth_is_blinking = true
    current_blink_count = 0
    current_earth_blink_time = 0.0
end

function spawnNewAsteroid()
    earth_x = love.graphics.getWidth() / 2
    earth_y = love.graphics.getHeight() / 2
    
    local x = 0.0
    local y = 0.0
    local side = random_number_generator:random(0, 3)
    
    if side == 0 then
        x = random_number_generator:random(0, love.graphics.getWidth())
        y = -5
    elseif side == 1 then
        x = random_number_generator:random(0, love.graphics.getWidth())
        y = love.graphics.getHeight() + 5
    elseif side == 2 then
        x = -5
        y = random_number_generator:random(0, love.graphics.getHeight())
    else
        x = love.graphics.getWidth() + 5
        y = random_number_generator:random(0, love.graphics.getHeight())
    end
    
    x_direction, y_direction = math.normalize((earth_x - x), (earth_y - y))

    asteroids[asteroid_count] = {}
    asteroids[asteroid_count].active = true
    asteroids[asteroid_count].position = {}
    asteroids[asteroid_count].direction = {}
    asteroids[asteroid_count].position.x = x
    asteroids[asteroid_count].position.y = y
    asteroids[asteroid_count].direction.x = x_direction
    asteroids[asteroid_count].direction.y = y_direction
    asteroids[asteroid_count].animation = newAnimation(loadTexture("assets/textures/asteroid_sheet.png"), 32, 32, 1.0)
    asteroid_count = asteroid_count + 1
end

function spawnExplosion(r, g, b, x, y)
    explosion_sound:play()
    explosions[explosion_count] = {}
    explosions[explosion_count].active = true
    explosions[explosion_count].particle_count = random_number_generator:random(10, 25)
    explosions[explosion_count].particles = {}

    for i = 0, explosions[explosion_count].particle_count - 1 do
        explosions[explosion_count].particles[i] = {}
        explosions[explosion_count].particles[i].position = {}
        explosions[explosion_count].particles[i].direction = {}
        explosions[explosion_count].particles[i].position.x = x
        explosions[explosion_count].particles[i].position.y = y
        direction_x, direction_y = math.normalize(random_number_generator:random(-100.0, 100.0) / 100.0, random_number_generator:random(-100.0, 100.0) / 100.0)
        explosions[explosion_count].particles[i].direction.x = direction_x
        explosions[explosion_count].particles[i].direction.y = direction_y
    end

    explosions[explosion_count].color = {}
    explosions[explosion_count].color.r = r
    explosions[explosion_count].color.g = g
    explosions[explosion_count].color.b = b
    explosions[explosion_count].particle_speed = 300
    explosions[explosion_count].total_time = 0.35
    explosions[explosion_count].current_time = 0.0
    explosion_count = explosion_count + 1
end

-- Animation stuff
function newAnimation(image, width, height, duration)
    local animation = {}
    animation.spriteSheet = image
    animation.quads = {}
 
    for y = 0, image:getHeight() - height, height do
        for x = 0, image:getWidth() - width, width do
            table.insert(animation.quads, love.graphics.newQuad(x, y, width, height, image:getDimensions()))
        end
    end
 
    animation.duration = duration or 1
    animation.currentTime = 0
 
    return animation
end

-- Collision

-- Collision detection function;
-- Returns true if two boxes overlap, false if they don't;
-- x1,y1 are the top-left coords of the first box, while w1,h1 are its width and height;
-- x2,y2,w2 & h2 are the same, but for the second box.
function CheckCollision(x1, y1, w1, h1, x2, y2, w2, h2)
  return x1 < x2 + w2 and
         x2 < x1 + w1 and
         y1 < y2 + h2 and
         y2 < y1 + h1
end

function startShake(duration, magnitude)
    t, shakeDuration, shakeMagnitude = 0, duration or 0.5, magnitude or 2
end